package com.example.factsaboutanimals;

import com.example.factsaboutanimals.ResponseFact;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://cat-fact.herokuapp.com/facts/";
    private static PrivateApi privateApi;
    public static final int AMOUNT = 100;

    public interface PrivateApi {
        @GET("random")
        Observable <List<ResponseFact>> getFacts (@Query("amount") int amount, @Query("animal_type") String animal_type);
    }

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }


    public static Observable<List<ResponseFact>> getFact(String animal_type) {
        return privateApi.getFacts(AMOUNT, animal_type);
    }
}
