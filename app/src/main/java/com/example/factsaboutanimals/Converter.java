package com.example.factsaboutanimals;

import java.util.ArrayList;
import java.util.List;

public class Converter {
    public static List<Fact> convert (List<ResponseFact> responceFacts){
        List<Fact> facts = new ArrayList<>();
        for (int i = 0; i < responceFacts.size(); i++) {
            facts.add(new Fact(i + 1, responceFacts.get(i).text));
        }
        return facts;


    }
}
