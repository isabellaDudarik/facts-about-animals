package com.example.factsaboutanimals;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class FactDao {
    @Insert
    public abstract void insertAll(List<Fact> facts);

    @Query("SELECT * FROM Fact")
    public abstract Flowable<List<Fact>> selectAll();

    @Query("DELETE FROM Fact")
    public abstract void removeAll();

    public void updateAll(List<Fact> facts) {
        removeAll();
        insertAll(facts);
    }
}
