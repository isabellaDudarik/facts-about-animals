package com.example.factsaboutanimals;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Fact {
    @PrimaryKey
    public int id;
    String fact;

    public Fact() {
    }

    public Fact(int id, String fact) {
        this.id = id;
        this.fact = fact;
    }
}
