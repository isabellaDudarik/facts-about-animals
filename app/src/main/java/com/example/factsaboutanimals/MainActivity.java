package com.example.factsaboutanimals;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    MyDataBase dataBase;
    Disposable disposable;
    CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        textView = findViewById(R.id.textView);

        dataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .fallbackToDestructiveMigration()
                .build();
        getinfo();
        printCountries();


    }

    private void printCountries() {
        disposables.add(
                dataBase.getFactDao().selectAll()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<List<Fact>>() {
                            @Override
                            public void accept(List<Fact> facts) throws Exception {
                                for (Fact fact : facts) {
                                    Log.d("data", fact.fact);

                                }
                                if (!facts.isEmpty()) {

                                    RecyclerView recyclerView = findViewById(R.id.list);
                                    AdapterFactsList adapter = new AdapterFactsList(MainActivity.this, facts);
                                    recyclerView.setAdapter(adapter);
//                                    textView.setText(facts.get(0).fact);
                                }
                                }
                        }));
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposables.clear();
    }


    public void getinfo() {
        disposable = ApiService.getFact("cat")
                .map(new Function<List<ResponseFact>, Boolean>() {
                    @Override
                    public Boolean apply(List<ResponseFact> responseModels) throws Exception {
                        List<Fact> facts = Converter.convert(responseModels);
                        dataBase.getFactDao().updateAll(facts);
                        return true;
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Boolean>() {
                               @Override
                               public void accept(Boolean aBoolean) throws Exception {
                                   /* DO NOTHING */
                               }
                           }

                        , new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        });
    }

}
