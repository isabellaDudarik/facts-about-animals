package com.example.factsaboutanimals;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

public class ResponseFact {
    public int id;
    public String text;
}
