package com.example.factsaboutanimals;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Fact.class}, version = 3)
public abstract class MyDataBase extends RoomDatabase {
    public abstract FactDao getFactDao();
}
