package com.example.factsaboutanimals;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterFactsList extends RecyclerView.Adapter<AdapterFactsList.MyViewHolder> {
    private Context context;
    private List<Fact> list;

    public AdapterFactsList(Context context, List<Fact> list) {
        this.context = context;
        this.list = list;
    }

    public AdapterFactsList(MainActivity context, String fact) {

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View rowView = layoutInflater.inflate(R.layout.recycler_view_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(rowView);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView factAnimal;
        ImageView imageAnimal;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            factAnimal = itemView.findViewById(R.id.factAnimal);
            imageAnimal = itemView.findViewById(R.id.imageAnimal);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "it's a cat", Toast.LENGTH_LONG).show();
                }
            });
        }

        public void bind(int position) {
            factAnimal.setText(list.get(position).fact);
//            Glide.with(context).load("http://rebzi.ru/UserFiles/festival/fe164cd130.jpg").into(imageAnimal);
            Picasso.get().load(R.drawable.cat).into(imageAnimal);
        }
    }
}
